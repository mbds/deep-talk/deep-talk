import React from 'react';
import './Home.scss';
import { trackPromise } from 'react-promise-tracker';
import { usePromiseTracker } from "react-promise-tracker";
import Loader from 'react-loader-spinner';
import 'bootstrap/dist/css/bootstrap.css';

import img1 from './resources/mbds.jpeg'
import img2 from './resources/uca.png'
import img3 from './resources/docaposte.jpeg'

const LoadingIndicator = props => {
       const { promiseInProgress } = usePromiseTracker();
    
       return (
         promiseInProgress && 
         <div
                  style={{
                    width: "100%",
                    height: "100",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Loader type="ThreeDots" color="#2BAD60" height="80" width="80" />
                </div>
      );  
     }

const Home = (props) => {
    const [selectedFile, setSelectedFile] = React.useState(null);
    const [source, setSource] = React.useState("file");
    const [textValue, setTextValue] = React.useState("");
    const [questionValue, setQuestion] = React.useState("");
    const [reponse, setReponse] = React.useState("");

    const fileChanged = (e) => {
        setSelectedFile(e.target.files[0]);
        console.log(selectedFile)
    }

    const sourceChanged = (e) => setSource(e.currentTarget.value);

    const textChanged = (e) => {
        setTextValue(e.currentTarget.value);
    }

    const questionChanged = (e) => setQuestion(e.currentTarget.value);

    const { promiseInProgress } = usePromiseTracker();

    const askQuestion = (e) => {
        // todo generate reponse
        setReponse("")
        if (textValue || selectedFile){
           if(questionValue){
                console.log(selectedFile)
                const data = new FormData();
                
                    data.append('file', selectedFile);

                    source == 'text' ? data.append('text', textValue) : data.append('text', "")

                    //data.append('text',textValue);
                
                data.append('question',questionValue);
                // fetch(`http://localhost:5000/tpt?question=${questionValue}&text=${textValue}`, { // Your POST endpoint
                //     method: 'POST',
                //     headers: {"Content-Type": "application/pdf"},
                //     body: selectedFile // This is your file object
                // }).then(response => response.json().then(data =>{
                //              setReponse(data.tpt)
                //             }))
                console.log(questionValue)
                trackPromise(
                    fetch(`http://localhost:5000/tpt`,{
                        method:'POST',
                        body: data
                    })
                    .then(response => response.json().then(data =>{
                        console.log(data.tpt)
                        setReponse(data.tpt)
                    })))
                        // const fileInput = selectedFile;
                        // const formData = new FormData();
        
                        // formData.append('file', selectedFile);
        
                        // const options = {
                        // method: 'POST',
                        // body: formData,
                        // // If you add this, upload won't work
                        // // headers: {
                        // //   'Content-Type': 'multipart/form-data',
                        // // }
                        // };
        
                        // fetch(`http://localhost:5000/tpt?question=${questionValue}&text=${textValue}`, options);
            }
            else{
                window.alert("Qestion Manquante !!");
            }
        }
        else
        {
            window.alert("Aucun fichier ni PDF n'a été uploadé !!");
        }
    };

    return (
        <div className="container-app">
            <div className="title">
                DeepTalk
            </div>
            <div className="titre2">1. Select source</div>
            <div className="container-step1">
                <div className="container-choices">
                    <div className="choice">
                        <div className="radiobtn"><input className="radiobtn" type="radio" name="source" value="file" encType="multipart/form-data" defaultChecked onChange={sourceChanged}/></div>
                        <div className="row2"><label className="row2" htmlFor="question">Upload a PDF :</label></div>
                    </div>
                    <div className="choice">
                    <div className="radiobtn"><input className="radiobtn" type="radio" name="source" value="text" onChange={sourceChanged}  /></div>
                        <div className="row2"><label className="row2" htmlFor="question">Saisir un texte :</label></div>
                       
                    </div>
                </div>
                <div className="container-inputs">
                    <div className={"container-auto-width " + (source === "text" ? 'overlay' : '') }>
                        <div className="cell-board-input-1">
                            <input type="file" id="file" onChange={fileChanged}/>
                            <label htmlFor="file" className="btn-1 upload-btn">{selectedFile ? selectedFile.name : "Upload"}</label>
                        </div>
                    </div>
                    <div className={"cell-board-input-2 " + (source === "file" ? 'overlay' : '') }>
                        <textarea className="text-area" onChange={textChanged} value={textValue}></textarea>
                    </div>
                </div>
            </div>
            <div className="titre2">2. Ask anything !</div>
            <LoadingIndicator/>
                    <div className="row2" >
                        <label htmlFor="question">Question :</label>
                    </div>
                    <div >
                    <input type="text" placeholder="Entrez votre question ?" className="question-input" onChange={questionChanged} value={questionValue}/>
                    </div>
                    <div className="row2">
                    <button className="btn-go-question" onClick={askQuestion}>Go !</button>
                    </div>
                    <div className="row2">
                    <label htmlFor="reponse">Réponse :</label>
                    </div>
                    <div className="response-input" >
                    <input type="text"  className="response-input" value={reponse} readOnly/>
                    </div>

                    <div className="footer">
                    <img src={img1} className="img1" />
                    <img src={img2} className="img2" />
                    <img src={img3} className="img3" />
                    </div>



        </div>
    )
};

export default Home;
