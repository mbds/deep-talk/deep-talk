from __future__ import print_function 
import os
import sys
from werkzeug.utils import secure_filename
from transformers import AutoTokenizer, AutoModelForQuestionAnswering
import tensorflow as tf
import torch
from flask import Flask,jsonify,request
import re
from flask_cors import CORS
#for pdf ananlysis
import io
from PIL import Image
import pytesseract
from wand.image import Image as wi
pytesseract.pytesseract.tesseract_cmd = r'C:\Tesseract-OCR\tesseract'

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

def pdf_to_text(filen):
    pdf = wi(filename = filen, resolution = 300)
    pdfImg = pdf.convert('jpeg')

    imgBlobs = []

    for img in pdfImg.sequence:
        page = wi(image = img)
        imgBlobs.append(page.make_blob('jpeg'))

    extracted_text = []

    for imgBlob in imgBlobs:
        im = Image.open(io.BytesIO(imgBlob))
        text = pytesseract.image_to_string(im, lang = 'fra')
        extracted_text.append(text)

    return extracted_text[0]

@app.route('/tpt', methods = ['POST'])
def hello_world():
    tokenizer = AutoTokenizer.from_pretrained("henryk/bert-base-multilingual-cased-finetuned-dutch-squad1")
    model = AutoModelForQuestionAnswering.from_pretrained("henryk/bert-base-multilingual-cased-finetuned-dutch-squad1")
    #question = "Quels services offrent Google?"

    if  request.form['text'] !="" :
        text=request.form['text']
        print("test4")

    elif request.files['file'] :
         print("test 3")
         file=request.files['file']
         filename = secure_filename(request.files['file'].filename)
         path = UPLOAD_FOLDER+'/'+filename
         if not os.path.exists(path) :
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
         #pdfReader = PyPDF2.PdfFileReader(pdfFileObject)   
         #count = pdfReader.numPages
         #text = ""
         # Reading a file 
         #f =request.files["file"].read()
         path = UPLOAD_FOLDER+'/'+filename
         print(path)
         print(filename)
         text = pdf_to_text(path)
            #read() 
         #######text = f.read(10)
         #for i in range(count):
         #   page = pdfReader.getPage(i)
         #  res +=page.extractText()
         #page = pdfReader.getPage(18)
         #text =page.extractText()
         ###############text =pdf_to_text(bytes(file)) 
         ###############################print('test44')
         #print (text)  
    question = request.form['question']
    print('test 4')
    #text = """Google LLC est une entreprise américaine de services technologiques fondée en 1998 dans la Silicon Valley, en Californie, par Larry Page et Sergey Brin, créateurs du moteur de recherche Google.C'est une filiale de la société Alphabet depuis août 2015.L'entreprise s'est principalement fait connaître à travers la situation monopolistique de son moteur de recherche, concurrencé historiquement par AltaVista puis par Yahoo! et Bing. Elle a ensuite procédé à de nombreuses acquisitions et développements et détient aujourd'hui de nombreux logiciels et sites web notables parmi lesquels YouTube, le système d'exploitation pour téléphones mobiles Android, ainsi que d'autres services tels que Gmail, Google Earth, Google Maps ou Google Play.Google s'est donné comme mission « d'organiser l'information à l'échelle mondiale et de la rendre universellement accessible et utile ». Après Larry Page et Eric Schmidt, son DG est, depuis 2015, Pichai Sundararajan."""
    input_dict = tokenizer.encode_plus(question, text, return_tensors="pt")
    print('test 5')
    input_ids = input_dict["input_ids"].tolist()
    print('test 6')
    start_scores, end_scores = model(**input_dict)
    print('test 7')
    all_tokens = tokenizer.convert_ids_to_tokens(input_ids[0])
    print('test 8')
    answer = ' '.join(all_tokens[torch.argmax(start_scores) : torch.argmax(end_scores)+1]).replace('▁', '')
    final_answer =  re.sub(r" ##","",answer)
    return {'tpt': final_answer}


@app.route('/api')
def generateAnswer(text,question):
    #henryk/bert-base-multilingual-cased-finetuned-dutch-squad2
    #henryk/bert-base-multilingual-cased-finetuned-dutch-squad1
    tokenizer = AutoTokenizer.from_pretrained("henryk/bert-base-multilingual-cased-finetuned-dutch-squad2")
    model = AutoModelForQuestionAnswering.from_pretrained("henryk/bert-base-multilingual-cased-finetuned-dutch-squad2")
    input_dict = tokenizer.encode_plus(question, text, return_tensors="pt")
    input_ids = input_dict["input_ids"].tolist()
    start_scores, end_scores = model(**input_dict)

    all_tokens = tokenizer.convert_ids_to_tokens(input_ids[0])
    answer = ' '.join(all_tokens[torch.argmax(start_scores) : torch.argmax(end_scores)+1]).replace('▁', '')
    final_answer =  re.sub(r" ##","",answer)
    return final_answer

